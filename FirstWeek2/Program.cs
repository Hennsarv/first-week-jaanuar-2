﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FirstWeek2
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\resultaadid.txt";
            string[] loetudread = File.ReadAllLines(filename);

            Dictionary<string, int> summad = new Dictionary<string, int>();
            Dictionary<string, List<int>> punktid = new Dictionary<string, List<int>>();

            foreach (var x in loetudread)
            {
                string[] osad = x.Split(' ');
                string nimi = osad[0].ToLower();
                int punkt = int.Parse(osad[1]);
                if (!punktid.Keys.Contains(nimi)) punktid.Add(nimi, new List<int>());
                punktid[nimi].Add(punkt);
            }
            foreach (var x in punktid)
                summad.Add(x.Key, x.Value.Sum());

            List<int> summ2 = summad.Values.ToList();
            summ2.Sort();
            summ2.Reverse();
            
            string kes = "*";
            while(kes != "")
            {
                Console.Write("Võistleja nimi: ");
                kes = Console.ReadLine().ToLower();
                if (kes != "")
                {
                    string kes2 = kes.Substring(0, 1).ToUpper() + kes.Substring(1);
                    if (punktid.Keys.Contains(kes))
                    {
                        Console.Write($"Võistleja {kes2} punktid: " );
                        foreach (var x in punktid[kes])
                        {
                            Console.Write($"{x} ");
                        }
                        Console.WriteLine($"ja punktisumma {summad[kes]}");
                        for (int i = 0; i < summ2.Count; i++)
                        {
                            if (summ2[i] == summad[kes])
                            {
                                Console.WriteLine($"Kokku tuli ta {i+1}. kohale");
                                break;
                            }
                        }
                    }
                    else
                        Console.WriteLine($"{kes2} ei osalenud võistlusel");
                }
            }

        }
    }
}
